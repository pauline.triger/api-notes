package model;

import jakarta.persistence.*;

import java.time.LocalDateTime;
import java.util.UUID;

@Entity
@Table(name = "notes")
public class Note {

    @Id
    @GeneratedValue
    private UUID id;

    @Column(name = "title")
    private String title;

    @Column(name = "datetime")
    private LocalDateTime datetime;

    @Column(name = "content")
    private String content;

    @ManyToOne
    private Folder folder;

    public Note(String title, String content, Folder folder) {
        super();
        this.title = title;
        this.content = content;
        this.datetime = LocalDateTime.now();
        this.folder = folder;
    }

    protected Note() {
    }

    private Note(UUID id, String title, String content, Folder folder) {
        this.id = id;
        this.title = title;
        this.content = content;
        this.datetime = LocalDateTime.now();
        this.folder = folder;
    }

    public static Note hydrate(UUID id, String title, String content, Folder folder) {
        return new Note(id, title, content, folder);
    }

    public UUID getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public LocalDateTime getDatetime() {
        return datetime;
    }

    public void setDatetime() {
        this.datetime = LocalDateTime.now();
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Folder getFolder() {
        return this.folder;
    }

    public void setFolder(Folder folder) {
        this.folder = folder;
    }
}