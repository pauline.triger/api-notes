package model;

import jakarta.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "folders")
public class Folder {

    @Id
    @GeneratedValue
    private UUID id;

    @Column(name = "name")
    private String name;

    public Folder(String name){
        this.name = name;
    }

    private Folder(UUID id, String name) {
        this.id = id;
        this.name = name;
    }

    protected Folder() {}

    public UUID getId(){
        return id;
    }

    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name = name;
    }
}
