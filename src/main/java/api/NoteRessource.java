package api;

import controllers.FolderController;
import controllers.NoteController;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.UriInfo;
import model.Note;

import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDateTime;
import java.time.chrono.ChronoLocalDateTime;
import java.time.format.DateTimeFormatterBuilder;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@ApplicationScoped
@Path("/folders/{folderId}/notes")
public class NoteRessource {

    @Inject
    private NoteController noteController;

    @Path("/{noteId}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Note getNote(@PathParam("noteId") UUID noteId) {
        return noteController
                .getById(noteId)
                .orElseThrow(NotFoundException::new);
    }

    @Path("/{noteId}")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Note updateNote(@PathParam("noteId") UUID noteId, NoteJsonInput json) {
        var note = noteController
                .getById(noteId)
                .orElseThrow(NotFoundException::new);

        return noteController.updateNote(note, json.title, json.content);
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createNote(@Context UriInfo uriInfo, NoteJsonInput json, @PathParam("folderId") UUID folderId) throws URISyntaxException {
        var note = noteController.createNote(json.title, json.content, folderId);
        var uri = uriInfo.getPath() + note.getId();

        return Response
                .created(new URI(uri))
                .entity(note)
                .build();
    }

    @Path("/{noteId}")
    @DELETE
    public void deleteNote(@PathParam("noteId") UUID noteId) {
        var note = noteController
                .getById(noteId)
                .orElseThrow(NotFoundException::new);

        noteController.delete(note);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Note> getAll(@PathParam("folderId") UUID folderId) {
        return noteController.getFromFolder(folderId);
    }

}