package api;

import controllers.ExistentNameException;
import controllers.FolderController;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.UriInfo;
import model.Folder;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.UUID;

@ApplicationScoped
@Path("/folders")
public class FolderRessource {
    @Inject
    private FolderController folderController;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public List<Folder> getAllFolders() {
        return this.folderController.getAll();
    }

    @Path("/{folderId}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Folder getFolder(@PathParam("folderId") UUID folderId) {
        return folderController
                .getById(folderId)
                .orElseThrow(NotFoundException::new);
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createFolder(@Context UriInfo uriInfo, FolderJsonInput json) throws URISyntaxException {
        var folder = folderController.createFolder(json.name);
        var uri = uriInfo.getPath() + folder.getName();

        return Response
                .created(new URI(uri))
                .entity(folder)
                .build();
    }

    @Path("/{folderId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @PUT
    public Folder updateName(@PathParam("folderId") UUID folderId, FolderJsonInput json) throws ExistentNameException {
        var folder = folderController
                .getById(folderId)
                .orElseThrow(NotFoundException::new);

        return folderController.updateName(folder, json.name);
    }

    @Path("/{folderId}")
    @DELETE
    public void deleteFolder(@PathParam("folderId") UUID folderId) {
        var folder = folderController
                .getById(folderId)
                .orElseThrow(NotFoundException::new);

        folderController.delete(folder);
    }
}
