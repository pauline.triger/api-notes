package dao;

import model.Folder;
import model.Note;

import java.util.List;
import java.util.UUID;

public interface FolderDAO {
    List<Folder> getAll();
    Folder getById(UUID id);
    void persist(Folder folder);
    Folder getByName(String name);
    void delete(Folder folder);
    void update(Folder folder, String name);
}
