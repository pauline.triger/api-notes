package dao;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import model.Folder;
import model.Note;

import java.util.List;
import java.util.UUID;

@ApplicationScoped
public class FolderJpaDAO implements FolderDAO {
    @PersistenceContext
    private EntityManager em;

    @Override
    public List<Folder> getAll() {
        var jpql = "SELECT f FROM Folder f";
        return em.createQuery(jpql, Folder.class)
                .getResultList();
    }

    @Override
    public Folder getByName(String name) {
        var jpql = "SELECT f FROM Folder f WHERE f.name = :name";
        return em.createQuery(jpql, Folder.class)
                .setParameter("name", name)
                .getSingleResult();
    }

    @Override
    public Folder getById(UUID id) {
        var jpql = "SELECT f FROM Folder f WHERE f.id = :id";
        return em.createQuery(jpql, Folder.class)
                .setParameter("id", id)
                .getSingleResult();
    }

    @Override
    public void persist(Folder folder) {
        em.persist(folder);
    }

    @Override
    public void update(Folder folder, String name) {
        folder.setName(name);
        em.merge(folder);
    }

    @Override
    public void delete(Folder folder) {
//        Pour supprimer toutes les notes d'un dossier
//        for (int i = 0; i < notes.size(); i++) {
//            em.remove(em.merge(notes.get(i)));
//        }
        em.remove(em.merge(folder));
    }
}
