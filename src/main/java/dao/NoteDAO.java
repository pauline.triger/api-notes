package dao;

import model.Folder;
import model.Note;

import java.util.List;
import java.util.UUID;

public interface NoteDAO {
    void persist(Note note);
    List<Note> getAllNotesFromFolder(Folder folder);
    Note getById(UUID noteId);
    void updateNote(Note note, String title, String content);
    void delete(Note note);
}
