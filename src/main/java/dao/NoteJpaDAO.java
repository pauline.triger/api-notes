package dao;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import model.Folder;
import model.Note;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@ApplicationScoped
public class NoteJpaDAO implements NoteDAO{

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<Note> getAllNotesFromFolder(Folder folder){
        var jpql = "SELECT n FROM Note n WHERE n.folder = :folder_id" ;
        return em.createQuery(jpql, Note.class)
                .setParameter("folder_id", folder)
                .getResultList();
    }

    @Override
    public Note getById(UUID id) {
        var jpql = "SELECT n FROM Note n WHERE n.id = :id";
        return em.createQuery(jpql, Note.class)
                .setParameter("id", id)
                .getSingleResult();
    }

    @Override
    public void updateNote(Note note, String title, String content) {
        note.setTitle(title);
        note.setContent(content);
        note.setDatetime();
        em.merge(note);
    }

    @Override
    public void delete(Note note) {
        em.remove(em.merge(note));
    }

    @Override
    public void persist(Note note) {
        em.persist(note);
    }
}
