package controllers;

import model.Folder;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface FolderController {
    List<Folder> getAll();
    Folder createFolder(String name);
    void delete(Folder folder);
    Folder updateName(Folder folder, String name) throws ExistentNameException;
    Optional<Folder> getById(UUID folderId);

}
