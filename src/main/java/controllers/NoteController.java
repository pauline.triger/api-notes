package controllers;

import model.Note;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface NoteController {
    Note createNote(String title, String content, UUID folderId);
    List<Note> getFromFolder(UUID folderId);
    Optional<Note> getById(UUID noteId);
    Note updateNote(Note note, String title, String content);
    void delete(Note note);
}
