package controllers;

import dao.FolderDAO;
import dao.NoteDAO;
import dao.NoteJpaDAO;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.BadRequestException;
import model.Folder;
import model.Note;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@ApplicationScoped
public class FolderControllerImpl implements FolderController {
    @Inject
    private FolderDAO folderDAO;

    @Override
    public List<Folder> getAll() {
        return folderDAO.getAll();
    }

    @Override
    public Optional<Folder> getById(UUID folderId) {
        var maybeFolder = folderDAO.getById(folderId);
        return Optional.ofNullable(maybeFolder);
    }

    @Override
    @Transactional
    public Folder createFolder(String name) {
        var folder = new Folder(name);
        folderDAO.persist(folder);
        return folder;
    }

    @Override
    @Transactional
    public Folder updateName(Folder folder, String name) throws ExistentNameException {
        try{
            folderDAO.update(folder,name);

            return folderDAO.getByName(name);
        }
        catch (Exception e){
            throw new BadRequestException();
        }
    }

    @Override
    @Transactional
    public void delete(Folder folder) {
        folderDAO.delete(folder);
    }
}
