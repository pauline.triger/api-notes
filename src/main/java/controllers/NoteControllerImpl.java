package controllers;

import dao.FolderDAO;
import dao.NoteDAO;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.transaction.Transactional;
import jakarta.ws.rs.BadRequestException;
import model.Folder;
import model.Note;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@ApplicationScoped
public class NoteControllerImpl implements NoteController{

    @Inject
    private NoteDAO noteDAO;

    @Inject
    private FolderDAO folderDAO;

    @Override
    @Transactional
    public Note createNote(String title, String content, UUID folderId) {
        Folder folder = folderDAO.getById(folderId);
        var note = new Note(title, content, folder);
        noteDAO.persist(note);
        return note;
    }

    @Override
    public Optional<Note> getById(UUID noteId) {
        var maybeNote = noteDAO.getById(noteId);
        return Optional.ofNullable(maybeNote);
    }

    @Override
    @Transactional
    public Note updateNote(Note note, String title, String content) {
        try{
            noteDAO.updateNote(note,title,content);
            return noteDAO.getById(note.getId());
        }
        catch (Exception e){
            throw new BadRequestException();
        }
    }

    @Override
    @Transactional
    public void delete(Note note) {
        noteDAO.delete(note);
    }

    @Override
    public List<Note> getFromFolder(UUID folderId) {
        Folder folder = folderDAO.getById(folderId);
        return noteDAO.getAllNotesFromFolder(folder);
    }
}
